
#include <iostream>

class Demo{

	public:
	int x = 10;
	static int y;

	void info();
};

int Demo::y = 20;

void Demo::info(){

	std::cout<< "x = " << x <<std::endl;
	std::cout<< "y = " << y <<std::endl;
}

int main(){

	Demo obj1;
	Demo obj2;

	obj1.info();
	obj2.info();

	obj1.x = 50;
	obj1.y = 100;

	obj1.info();
	obj2.info();
}
