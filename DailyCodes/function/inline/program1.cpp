
#include <iostream>
#define sum(x,y) x+y
#define z 500

int main(){

	int x = 10;
	int y = 30;

	std::cout<< sum(x,y) <<std::endl;	//replaceses sum(x,y) by x+y at preprocessing level
	std::cout<< z <<std::endl;		//replaceses z by 500 at preprocessing level

	return 0;
}
