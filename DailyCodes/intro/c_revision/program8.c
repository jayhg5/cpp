
#include <stdio.h>

struct Batsman{

	char name[20];
	int jerNo;
	double avg;

}vk = {"Virat",18,48.50};

void main(){

	struct Batsman obj = {"Mahi",7,60.00};

	struct Batsman *vptr = &vk;
	struct Batsman *mptr = &obj;
	
	printf("%s\n",vptr->name);
	printf("%d\n",vptr->jerNo);
	printf("%lf\n",vptr->avg);
	
	printf("%s\n",(*mptr).name);
	printf("%d\n",(*mptr).jerNo);
	printf("%lf\n",(*mptr).avg);
}
