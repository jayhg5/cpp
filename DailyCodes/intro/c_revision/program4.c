#include <stdio.h>

void fun(int row, int col, int (*arr)[row][col]){

        for(int i=0; i<2; i++){
                for(int j=0; j<row; j++){
                	for(int k=0; k<col; k++){
                        	printf("%d ",*(*(*(arr+i)+j)+k));
			}
                	printf("\n");
                }
                printf("\n");
        }
}

void main(){

        int arr[][333][3] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};

        fun(3,3,arr);
}
