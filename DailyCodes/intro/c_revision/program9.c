
#include <stdio.h>
#include <stdlib.h>

struct Lang{

	char langName[10];
	char founder[20];
};

void fun(){

	struct Lang *ptr = (struct Lang *) malloc(sizeof(struct Lang));

	printf("Enter Language: ");
	gets(ptr->langName);
	
	printf("Enter Founder: ");
	gets(ptr->founder);

	puts(ptr->langName);
	puts(ptr->founder);
}

void main(){

	for(int i=0; i<3; i++){
	
		fun();
	}
}
