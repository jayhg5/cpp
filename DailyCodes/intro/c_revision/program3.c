#include <stdio.h>

void fun(int col, int (*arr)[col]){

	for(int i=0; i<col; i++){
		for(int j=0; j<col; j++){
			printf("%d ",*(*(arr+i)+j));
		}
		printf("\n");
	}
}

void main(){

	int arr[][3] = {1,2,3,4,5,6,7,8,9};

	fun(3,arr);
}
