
#include <iostream>

class Demo{

	int *ptrArray = NULL;

	public:
	Demo(){
		int *ptrArray = new int[50];

		for(int i=0; i<50; i++){
			*(ptrArray+i) = i+10;
		}
		std::cout<< "In Constructor" <<std::endl;
	}

	~Demo(){
		delete[] ptrArray;
		std::cout<< "In Destructor" <<std::endl;
	}
};

int main(){
	{
		Demo obj;
	}

	return 0;
}
