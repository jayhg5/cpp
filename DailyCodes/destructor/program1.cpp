
#include <iostream>

class Demo{

	public:
	int x = 10;

	Demo(){
		std::cout<< "In constructor" <<std::endl; 
		std::cout<< x <<std::endl;	
	}

	Demo(int x){
		this->x = x;
		std::cout<< "In para" <<std::endl;
		std::cout<< x <<std::endl;
		Demo();
	}

	~Demo(){
		std::cout<< "Destructor" <<std::endl;
	}
};

int main(){

	Demo obj(50);
	std::cout<< "In main" <<std::endl;

	return 0;
}
