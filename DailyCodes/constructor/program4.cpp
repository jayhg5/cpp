
#include <iostream>

class Demo{

	public:
	Demo(){
	
		std::cout<< "No-args constructor" <<std::endl;
	}

	Demo(int x){
	
		std::cout<< "Para constructor" <<std::endl;
		std::cout<< x <<std::endl;
	}
};

int main(){

	Demo obj1();	//function declaration
	Demo obj2{};	//call to 'No-args constructor'

	return 0;
}
