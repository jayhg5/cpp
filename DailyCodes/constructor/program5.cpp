// copy constructor

#include <iostream>

class Demo{

	public:
	Demo(){
		std::cout<< "No-args constructor" <<std::endl;
	}

	Demo(int x){
		std::cout<< "Para constructor" <<std::endl;
		std::cout<< x <<std::endl;
	}
	
	Demo(Demo &xyz){
		std::cout<< "copy constructor" <<std::endl;
	}
};

int main(){

	Demo obj1;		//No-args constructor
	Demo obj2(10);		//Para constructor
				//10
	Demo obj3(obj1);	//copy constructor
	Demo obj4 = obj1;	//copy constructor
}
