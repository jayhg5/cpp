
#include <iostream>

class Demo{

	int x = 10;
	int y = 20;

	public:
	Demo(){
		std::cout<< "No args" <<std::endl;
	}

	Demo(int x = 50, int y = 30){
		this->x = x;
		this->y = y;
		std::cout<< "Para" <<std::endl;
		std::cout<< x << " " << y <<std::endl;
	}
};

int main(){

	//Demo obj2;		//error: ambiguty
	Demo obj1(100);

	return 0;
}
