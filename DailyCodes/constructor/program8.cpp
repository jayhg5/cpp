
#include <iostream>

class Demo{

	public:
	int x = 10;

	Demo(){
		std::cout<< "No args" <<std::endl;
	}
	
	Demo(int x){
		std::cout<< "Para" <<std::endl;
	}
	
	Demo(Demo &obj){
		std::cout<< "Copy" <<std::endl;
	}
};

int main(){

	Demo obj1;
	Demo obj2 = obj1;

	std::cout<< obj1.x <<std::endl;
	std::cout<< obj2.x <<std::endl;
	
	obj2.x = 50;

	std::cout<< obj1.x <<std::endl;
	std::cout<< obj2.x <<std::endl;
}
