
#include <iostream>

int main(){

	int x = 10;
	int y = 20;

	const int *const ptr = &x;

	std::cout<< *ptr <<std::endl;

	ptr = &y;	//address in ptr can't be changed
	*ptr = 50;	//data in x can't be changed through pointer 'ptr'

	std::cout<< *ptr <<std::endl;
}
