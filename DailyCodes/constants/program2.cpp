
#include <iostream>

int main(){

	int x = 10;
	int y = 20;

	std::cout<< x << std::endl;	//10
	std::cout<< y << std::endl;	//20
	
	const int *ptr = &x;		// here data is constant (not pointer)

	std::cout<< *ptr << std::endl;	//10

	ptr = &y;

	std::cout<< *ptr << std::endl;	//20
}
