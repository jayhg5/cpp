
#include <iostream>

int& fun(int x){

	int y = x;
	return y;	//error: local reference variable returned
}

int main(){

	int a = 50;

	int ret = fun(a);

	std::cout<< ret <<std::endl;

	return 0;
}
