// Operator Overloading using Normal Function

#include <iostream>

class Demo{

	int x = 10;
	
	public:
	Demo(int x){
		this->x = x;
	}

	int getData() const {
		return this->x;
	}
};

int operator+(const Demo& obj1, const Demo& obj2){

	return obj1.getData() + obj2.getData();
}

int main(){

	Demo obj1(30);
	Demo obj2(50);

	std::cout<< obj1+obj2 <<std::endl;	//80

	return 0;
}
