
#include <iostream>

class Demo{

	int a = 10;

	public:
	Demo(){
	
	}
};

int main(){

	int x = 20;
	std::cout<< x <<std::endl;
	// internal function call --> operator<<(cout,x);
	// function prototype --> ostream& operator<<(ostream& cout, int x){}

	Demo obj;
	std::cout<< obj <<std::endl;	//error

	// internal function call --> operator<<(cout,obj);
	// function prototype --> ostream& operator<<(ostream& cout, Demo& obj){}

	return 0;
}
