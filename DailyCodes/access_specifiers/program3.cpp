
#include <iostream>

class Demo{

	int x = 10;

	public:
	int y = 20;

	protected:
	int z = 30;	
};

int main(){

	Demo obj;

	std::cout<< obj.x << obj.y <<obj.z <<std::endl;		//error for x & z (x is private and z is protected)
	return 0;
}
