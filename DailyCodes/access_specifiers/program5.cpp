#include <iostream>

class Demo{

	int x = 10;
	int y = 20;

	protected:
	int z = 30;

	public:
	void disp(){
	
		std::cout<< x << y << z <<std::endl;
	}
};

int main(){

	Demo obj;
	obj.disp();	//102030

	return 0;
}
