
#include <iostream>

class Player{

	int jerNo = 18;
	std::string name = "Virat Kohli";
	void disp(){
		std::cout<< jerNo <<std::endl;
		std::cout<< name <<std::endl;
	}
};

int main(){

	Player obj;
	
	std::cout<< obj.jerNo <<std::endl;	//error - private variable
	std::cout<< obj.name <<std::endl;	//error - private variable

	obj.disp();				//error - private function

	return 0;
}
