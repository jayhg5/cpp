
#include <iostream>

class Demo{

	int x = 10;

	public:
		void fun(){
			
			std::cout<< x <<std::endl;
		}
};

int main(){

	std::cout<< x << std::endl;		// x is declared in another scope (in class 'Demo')
	
	Demo obj;

	std::cout<< obj.x << std::endl;		// private variables can't be accessed from out side the class

	obj.fun();
}
