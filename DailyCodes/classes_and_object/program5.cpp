
#include <iostream>

class Demo{

	int x = 10;
	int y = 20;
	
	public:
	void fun(){
	
		std::cout<< x <<std::endl;
		std::cout<< y <<std::endl;
	}
};

int main(){

	Demo obj1;			//creating object

	Demo *obj2 = new Demo();	//creating object

	obj1.fun();
	obj2->fun();

	delete obj2;

	obj2->fun();			//garbage value
	
	return 0;
}
