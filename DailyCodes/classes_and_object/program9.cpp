
#include <iostream>

class Player{

	int jerNo = 18;
	std::string name = "Virat Kohli";

	public:
	void info(){		//internally --> void fun(Player *this){}
	
		std::cout<< jerNo << " = " << name <<std::endl;
	}
};

int main(){

	Player obj1;				//internally --> Player(&obj1);
	Player *obj2 = new Player();		//internally --> Player(obj2);

	obj1.info();				
	obj2->info();		//(*obj2).info();
	
	return 0;
}
