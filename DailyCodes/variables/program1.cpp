#include <iostream>

int main(){

	int x = 10;	//copy initilization
	
	int y(20);	//direct initilization

	std::cout<< x <<std::endl;
	std::cout<< y <<std::endl;
	
	int z{30};	//uniform initilization
	
	std::cout<< z <<std::endl;

	return 0;
}
