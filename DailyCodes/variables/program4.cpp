
#include <iostream>

int main(){

	int x = 10;

	std::cout<< x <<std::endl;	//10

	{
		int x = 20;
		
		std::cout<< x <<std::endl;	//20
		std::cout<< ::x <<std::endl;	//error (no gloabal 'x' declared (scope resolution operator '::' works only on global variable))
	
		x = 30;

		std::cout<< x <<std::endl;	//30
	}
	std::cout<< x <<std::endl;	//10
}
