
#include <iostream>

class Demo{

	int x = 10;
	static int y;	//error (in cpp we can't write static variable in class)

	public:

	void fun(){
	
		std::cout<< x <<std::endl;
		std::cout<< y <<std::endl;
	}
};

int main(){

	Demo obj;

	obj.fun();
	
	return 0;
}
