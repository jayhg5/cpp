
#include <iostream>

class Demo{

	int x = 10;

	protected:
	int y = 20;

	public:
	Demo(){
		std::cout<< "Constructor" <<std::endl;
	}

	void getData(){
		std::cout<< "x = " << x <<std::endl;
		std::cout<< "y = " << y <<std::endl;
	}

	friend void accessData(const Demo&);
};

void accessData(const Demo& obj){

	int temp = obj.x;	
	obj.x = obj.y;		//error
	obj.y = temp;		//error
}

int main(){

	Demo obj;
	obj.getData();
	accessData(obj);
}
