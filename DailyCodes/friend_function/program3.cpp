
#include <iostream>

class Two;

class One{

	int x = 10;

	protected:
	int y = 20;

	public:
	One(){
		std::cout<< "One Constructor" <<std::endl;
	}

	private:
	void getData() const {
		std::cout<< "x = " << x <<std::endl;
		std::cout<< "y = " << y <<std::endl;
	}

	friend void accessData(const One&, const Two&);
};

class Two{

	int a = 10;

	protected:
	int b = 20;

	public:
	Two(){
		std::cout<< "Two Constructor" <<std::endl;
	}

	private:
	void getData() const {
		std::cout<< "a = " << a <<std::endl;
		std::cout<< "b = " << b <<std::endl;
	}

	friend void accessData(const One&, const Two&);
};

void accessData(const One& obj1, const Two& obj2){

	obj1.getData();
	obj2.getData();
}

int main(){

	One obj1;
	Two obj2;
	accessData(obj1,obj2);
	
	return 0;
}
