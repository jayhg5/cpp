
#include <iostream>

class Demo{

	int x = 10;

	protected:
	int y = 20;

	public:
	Demo(){
		std::cout<< "Constructor" <<std::endl;
	}

	void getData(){
		std::cout<< "x = " << x <<std::endl;
		std::cout<< "y = " << y <<std::endl;
	}

	friend void accessData(const Demo&);
};

void accessData(const Demo& obj){

	std::cout<< "x = " << obj.x <<std::endl;
	std::cout<< "y = " << obj.y <<std::endl;
}

int main(){

	Demo obj;
	obj.getData();
	accessData(obj);
}
